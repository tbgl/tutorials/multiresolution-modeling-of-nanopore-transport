from pathlib import Path
import numpy as np
import scipy
from scipy import ndimage, interpolate
from writeDx import writeDx
try:
    from .parseComsol import parseComsolData
except:
    from parseComsol import parseComsolData

directory = Path(__file__).parent


def xyzrange(min_,max_,delta):
    return np.arange(min_-delta/2,max_+delta/2,delta)

def above_or_close(array, value):
    return ((array >= value) + np.isclose(array,value)) > 0
    
def loadGrids(name,xymin, zmin):
    datafile = "{}.txt".format(name)
    if not Path(datafile).exists():
        raise ValueError("Source file '{}' does not exist!".format(datafile))

    print("Reading",datafile)
    p_r,p_z,fields = parseComsolData(datafile)
    p_r = p_r*10
    p_z = p_z*10
    
    L = 3000
    #x = y = xyzrange(-xymin-50,xymin+50+10,10)
    x = y = np.arange(-xymin-50,xymin+50+10,10)
    z = np.arange(-zmin-50,L+zmin+50+10,10)

    dx,dy,dz = [a[1]-a[0] for a in (x,y,z)]
    X,Y,Z = np.meshgrid(x,y,z, indexing='ij')
    R = np.sqrt(X**2 + Y**2)

#    cos = X/R; sin = Y/R;
#    cos[np.where(np.isnan(cos))] = 0
#    sin[np.where(np.isnan(sin))] = 0
    
    eof_factor = 1/4 #1/16
    nan_val = 10000

    scale = 1
    pot_rz = -1 * eof_factor * scale * fields[0] * 23.06055 # convert meV to kcal_mol
#    vz = 1e-3 * fields[2] * scale
#    vr = 1e-3 * fields[1] * scale
    
    pot = scipy.interpolate.interpn((p_r,p_z), pot_rz,
                                    np.array((R.flatten(order='C'),
                                              Z.flatten(order='C'))).T,
                                    method='linear', 
                                    bounds_error = False,
                                    fill_value = nan_val)
#    Vr = scipy.interpolate.interpn((p_r,p_z), vr,
#                                    np.array((R.flatten(order='C'),
#                                              Z.flatten(order='C'))).T,
#                                    method='linear', 
#                                    bounds_error = False,
#                                    fill_value = nan_val)
#    Vz = scipy.interpolate.interpn((p_r,p_z), vz,
#                                    np.array((R.flatten(order='C'),
#                                              Z.flatten(order='C'))).T,
#                                    method='linear', 
#                                    bounds_error = False,
#                                    fill_value = nan_val)
    
    pot[np.where(np.isnan(pot))] = nan_val
#    Vr[np.where(np.isnan(Vr))] = nan_val
#    Vz[np.where(np.isnan(Vz))] = nan_val
    
    pot = pot.reshape(R.shape,order='C')
#    Vz = Vz.reshape(R.shape,order='C')
#    Vr = Vr.reshape(R.shape,order='C')
    conf = pot.copy()

    indices = above_or_close(conf, nan_val); 
#    Vr[indices] = 0
#    Vz[indices] = 0
    pot[indices] = 0

    conf[np.where(conf < nan_val)] = 0
#    Vx = Vr * cos
#    Vy = Vr * sin
    
    delta = np.array( (dx,dy,dz) )
    origin = np.array( (x[0], y[0], z[0]) )

#    mask = above_or_close(pot, nan_val)
#    dist = ndimage.morphology.distance_transform_edt( mask )
#    pot[mask] = (dist[mask]-10)*0.1+10
#
#    ## Subtract value at boundary
#    pot = pot - np.mean( pot[:,:,-1] )

    mask = above_or_close(conf, nan_val)
    dist = ndimage.morphology.distance_transform_edt( mask )
    # conf[mask] = (dist[mask]-10)*0.1+10
    conf[mask] = 100*dist[mask]**2 + 200
    ## Subtract value at boundary
    conf = conf - np.mean( conf[:,:,-1] )

#    return conf, pot, Vx, Vy, Vz, origin, delta
    return conf, pot, origin, delta

# name = 'push'
# xymin = 1500
# zmin = 3000
# #conf, pot, Vx, Vy, Vz, origin, delta = loadGrids(name,xymin,zmin)
# conf, pot, origin, delta = loadGrids(name,xymin,zmin)

# comb = conf + pot

# writeDx('confine_%s.dx' % name, conf, origin = origin, delta = delta, fmt = '%.6f')
# writeDx('electric_%s.dx' % name, pot, origin = origin, delta = delta, fmt = '%.6f')
# writeDx('conf_elec_%s.dx' % name, comb, origin = origin, delta = delta, fmt = '%.6f')
# #writeDx('velX_%s.dx' % name, Vx, origin = origin, delta = delta, fmt = '%.6f')
# #writeDx('velY_%s.dx' % name, Vy, origin = origin, delta = delta, fmt = '%.6f')
# #writeDx('velZ_%s.dx' % name, Vz, origin = origin, delta = delta, fmt = '%.6f')

name = 'fig5_COMSOL'
xymin = 1500
zmin = 3000
#conf, pot, Vx, Vy, Vz, origin, delta = loadGrids(name,xymin,zmin)
conf, pot, origin, delta = loadGrids(name,xymin,zmin)

comb = conf + pot
#comb = 100*comb

#writeDx('confine_%s.dx' % name, conf, origin = origin, delta = delta, fmt = '%.6f')
#writeDx('electric_%s.dx' % name, pot, origin = origin, delta = delta, fmt = '%.6f')
writeDx('conf_elec.dx', comb, origin = origin, delta = delta, fmt = '%.6f')
# writeDx('velX_%s.dx' % name, Vx, origin = origin, delta = delta, fmt = '%.6f')
# writeDx('velY_%s.dx' % name, Vy, origin = origin, delta = delta, fmt = '%.6f')
# writeDx('velZ_%s.dx' % name, Vz, origin = origin, delta = delta, fmt = '%.6f')
