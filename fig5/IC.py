from mrdna import SingleStrandedSegment, DoubleStrandedSegment, SegmentModel
from mrdna.arbdmodel.coords import readArbdCoords
from mrdna.simulate import minimize_and_simulate_oxdna
from mrdna.model.dna_sequence import m13
from mrdna.readers import read_cadnano
import numpy as np

steps1 = 60e6
steps2 = 2e9
tstep1 = 1e-4
tstep2 = tstep1

center = np.array((0,0,0))

for run_num in range(1):
    model = read_cadnano('DFHR2.json')
    
    #dim = [max(x,1000) for x in model.dimensions_from_structure(padding_factor=2.5)]
    #model.dimensions =  dim
    model.dimensions =  np.array([3000,3000,20000])
    model.origin = model.get_center()-0.5*np.array(model.dimensions)
    
    model.generate_bead_model()
    model.simulate(
        output_name = "loop1", 
        directory='output{:02d}'.format(run_num),
        gpu = 1,
        num_steps=steps1, 
        output_period=1e4, 
        timestep=tstep1)

    model.grid_potentials = []
    model.add_grid_potential("conf_elec.dx", scale=1.0, per_nucleotide = True)
    model.generate_bead_model()
#    model.generate_bead_model(
#        max_basepairs_per_bead = 4, 
#        max_nucleotides_per_bead = 4, 
#        local_twist=False)
    model.simulate(
        dry_run=True,
        output_name = "run", 
        directory='output{:02d}'.format(run_num),
        gpu = 1,
        num_steps=steps2, 
        output_period=1e4, 
        timestep=tstep2)


#for run_num in range(0,1):
#    dim = 5000 
#    sep = 17
#    num_dbs = 6
#    nbp = 2*(sep//2+1)+(sep+1)*(num_dbs-1)
#    # Set up the model in the box.
###########################    
#    segMain = DoubleStrandedSegment(
#        name="helix1",
#        start_position = center + np.array((0,0,-0.2*nbp)), 
#        end_position = center + np.array((0,0,0.2*nbp)), 
###########################
#        num_bp = nbp)
#
#    model = SegmentModel( [segMain],
#        local_twist = False,
#        max_basepairs_per_bead = n1,
#        max_nucleotides_per_bead = n1,
#        dimensions=(dim,dim,dim))
#    model.grid_potentials = [] # clear any existing potential
#
#    model.add_grid_potential("bc.dx", scale=1.0, per_nucleotide = True)
#        # This line shouldn't be necessary but add_grid_potentials doesn't actually add the thing automatically so this is a temporary hack
#    
#    
#    # Run the first coarse simulation, update the model spline with simulation results
#
#    model.generate_bead_model(
#        max_basepairs_per_bead = n1,
#        max_nucleotides_per_bead = n1, 
#        local_twist=False,
#        escapable_twist=False)
#    model.simulate(
#        output_name = "loop1", 
#        directory='output{:02d}' .format(run_num),
#        num_steps=steps1, 
#        output_period=1e4, 
#        timestep=tstep1)
#    
#    coords = readArbdCoords('output{:02d}/output/loop1.restart' .format(run_num))
#    model.update_splines(coords) # Fits a spline through the coordinates
#    
#    # Run the fifth finer simulation, update the coordinates again
#    
#    segMain = DoubleStrandedSegment(
#        name="main",
#        start_position = np.array((0,dim/4-500,0)), 
#        end_position = np.array((0,-dim/4+500,0)), 
###########################
#        num_bp = nbp)
#    segMain.sequence = m13[:segMain.num_nt]
#    segs = []
#    
#    starts = [] 
#    offset = sep
#
### This section generates the locations of the attachments for dumbbell pairs
##################
#    l = 14
#    s = 58
#    pos = offset//2
#
#    num_barages = 1
#    skip = sep + 1
#    for i in range(num_barages):
#        for j in range(num_dbs):
#            starts.append(pos)
#            pos += skip
#        pos += s - skip
##########
## Now we attach the dumbbells we want at the locations specified above.
#
#    sfrac = np.array(starts)/nbp
#    print(starts)
# 
#    main = model.segments[0]
#    pos_start = main.contour_to_position(sfrac)
#    j=0
#    for i in range(len(starts)):
#        segs = segs + makeDumbbellAA(segMain,pos_start[j],starts[j],l)
#        j+=1
#    
#    for i in range(len(starts)-1):
#        segMain.add_nick(starts[i]+1+sep//2, on_fwd_strand=True)
#
##########
## Now we set up and run the final m del
#
#    final = SegmentModel([segMain]+segs,
#        local_twist = True,
#        max_basepairs_per_bead = n5,
#        max_nucleotides_per_bead = n5,
#        dimensions=(dim,dim,dim))
#
#    print(final.segments)    
#
#    final.grid_potentials = []
#    final.add_grid_potential("bc.dx", scale=1.0, per_nucleotide = True)
#
#    #final.update_splines(coords)
#    final.segments[0].position_spline_params = main.position_spline_params
#    # if previous sim had twist
#    #final.segments[0].quaternion_spline_params = main.quaternion_spline_params
#
#    final.generate_bead_model(
#        max_basepairs_per_bead = n5,
#        max_nucleotides_per_bead = n5, 
#        local_twist=True, 
#        escapable_twist=False,
#        one_bead_per_monomer = True)
#
#
#    final.simulate(output_name = "loop5",
#        directory='output{:02d}' .format(run_num),
#        num_steps=steps5,
#        output_period=1e4,
#        gpu = 0,
#        timestep=tstep5)
#    coords = readArbdCoords('output{:02d}/output/loop5.restart' .format(run_num))
#    final.update_splines(coords) # Fits a spline through the coordinates
#
#    steps6 = 1e6 # 60e6
#    ts6 = 0.002 
#
#    final.generate_atomic_model(scale=0.25) # scale shrinks the backbone and that's evidently useful?
#    final.atomic_simulate(output_name='sixdbs',dry_run=True)
#    final.write_atomic_ENM("sixdbs",interhelical_bonds=False)
#
#    # Run the oxDNA simulation
#    output_name = "sixdbs"
#    top,conf = minimize_and_simulate_oxdna( final, 
#                                            output_name = output_name,
#                                            num_min_steps = 1e3,
#                                            num_steps = steps6,
#                                            output_period = 1e3,
#                                            timestep=ts6,
#                                            # oxDNA = '/data/server4/cmaffeo2/systems/origami-projects/tetrahedral-robot-2020/oxDNA-bin/oxDNA',
#                                            oxDNA = '/data/server7/hchou10/oxDNA/oxDNA/build/bin/oxDNA',
#                                            seq_dep_file = 'oxDNA2'
#    )
