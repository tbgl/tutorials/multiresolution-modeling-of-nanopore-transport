#source ../../capillary/capi.procs.tcl

proc seq { s f {inc 1}} {
    # builds s..f
    if { $inc <= 0 } { error "seq: inc (third arg) must be positive!"; return }
    if { $s > $f } { set inc [expr -1*$inc] }
    for { } { ($f-$s)*$inc >= 0 } { set s [expr $s+$inc] } { lappend ret $s }
    return "$ret"
    }


set dia "50" 
set volt "600" 
set sim "00"

foreach sim {"00"} {

    set zf [open "${volt}_${dia}_ends.txt" a]
    
    mol new ../run.psf
    set dcds [lsort [glob -type f run*dcd]]
    foreach file $dcds {
        mol addfile $file waitfor all
    }
    
    set max [molinfo top get numframes]
    set f 0
    set num_first 1
    set num_slit 1
    set start 0
    while {$f <= $max && $num_slit > 0} {
        set f [expr $f+1]
        set num_slit [[atomselect top "z > 0 and z < 3000 and x^2+y^2 < (0.0852053*z+255)^2" frame $f] num]
    }
    set finish [expr $f-1]
    
    puts $zf "$sim\t$start\t$finish"
    close $zf
    
    set step 150
    set first [expr $start]
    set last [expr $finish+$step]
    set condfile [open "${dia}_${volt}_${sim}_${step}.txt" w]
    
    set frames [seq $first $last $step]
    puts $frames

    foreach ff $frames {
        set d_cap [atomselect top "z > 0 and z < 3000 and x^2+y^2 < (0.0852053*z+255)^2" frame $ff]
        
        if {[$d_cap num] > 0} { 
            set x_cap [vecscale .1 [$d_cap get x]]
            set y_cap [vecscale .1 [$d_cap get y]]
            set z_cap [vecscale .1 [$d_cap get z]]
            
            puts $condfile $x_cap
            puts $condfile $y_cap
            puts $condfile $z_cap
        } else {
            puts $condfile ""
            puts $condfile ""
            puts $condfile ""
        } 
    }
    close $condfile

}
