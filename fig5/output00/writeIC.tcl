#source ../../capillary/capi.procs.tcl

proc typenum {ptypes name} {
    set var [lsearch $ptypes $name]
    return $var
}

# for the first restart
set frame [expr 6000-1]
#set frame [expr 2-1]
set dir output

set th  [expr acos(1-2*rand())]
set ph  [expr 2*3.14*rand()]
set rot [expr 2*3.14*rand()]
#set th  1.94551
#set ph  1.06257
#set rot 1.44587
set vec [list [expr sin($th)*cos($ph)] [expr sin($th)*sin($ph)] [expr cos($th)] ]

set ptypes {D000 D001 D002 D003 D004 D005 D006 D007 D008 D009 D010 D011 D012 S000 S001 S002 S003 S004 S005 S006 S007 S008 S009 S010}
foreach run { 0 } {
    set numm [format %02d $run]

    mol new loop1.psf
    mol addfile $dir/loop1.dcd first $frame last $frame waitfor all 
    
    set all [atomselect top "all"]
    lassign [measure inertia $all] c PAs 
    $all moveby [vecinvert $c]
    $all move [transabout $vec $rot rad]
    set shift [expr [lindex [lindex [measure minmax $all] 0] 2]*-1 + 3000]
    $all moveby [list 0 0 $shift]
    
    set pos [$all get {x y z}]
    set indexes [$all get index]
    set types [$all get type]
    
    set zf [open start.txt w]
    
    foreach index $indexes {
        set num [typenum $ptypes [lindex $types $index]]
        puts $zf "$num [lindex $pos $index]"
    }
    
    close $zf
}

exec /bin/sed -i "/inputParticle/s/.*/restartCoordinates start.txt/" run.bd
