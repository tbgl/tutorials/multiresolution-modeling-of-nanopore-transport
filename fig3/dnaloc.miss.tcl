proc seq { s f {inc 1}} {
    # builds s..f
    if { $inc <= 0 } { error "seq: inc (third arg) must be positive!"; return }
    if { $s > $f } { set inc [expr -1*$inc] }
    for { } { ($f-$s)*$inc >= 0 } { set s [expr $s+$inc] } { lappend ret $s }
    return "$ret"
    }


#set sim 1171
#set sim 957
#set sim 1863
#set sim 684
set sim 14
set step 10

set volt "250" 
set Pfile [open "${volt}_${sim}_${step}_P.txt" w]
set Bfile [open "${volt}_${sim}_${step}_B.txt" w]

cd /data/server5/schao3/small_dual_pore/2-simulation/output

mol new ../../1-system/V_res1A_dis15nm.grid.system.dx
# Frames that are captured even at the end: 1171 1479 1673
#foreach i [seq 1 2000] {
#foreach i {1171 1479 1673} {
foreach i $sim {
    mol new ../../1-system/dsDNA.bonded.psf
    mol addfile pull_ens_150nt_15nm_f20/pull_ens_150nt_15nm_f20_$i-1.dcd waitfor all
}


set first 0
set last 1400

set frames [seq $first $last $step]

foreach ff $frames {
    set d_cap [atomselect top "type P" frame $ff]
    # get the z coefficient from tan(cone angle) and the constant is just 5 more than the tip radius.
    # cone angle is 0.085 radians and tip radius is 20 (Angstroms)
    if {[$d_cap num] > 0} { 
        set x_cap [vecscale .1 [$d_cap get x]]
        set y_cap [vecscale .1 [$d_cap get y]]
        set z_cap [vecscale .1 [$d_cap get z]]
        
        puts $Pfile $x_cap
        puts $Pfile $y_cap
        puts $Pfile $z_cap
    } else {
        puts $Pfile ""
        puts $Pfile ""
        puts $Pfile ""
    } 
}

foreach ff $frames {
    set d_cap [atomselect top "type B" frame $ff]
    # get the z coefficient from tan(cone angle) and the constant is just 5 more than the tip radius.
    # cone angle is 0.085 radians and tip radius is 20 (Angstroms)
    if {[$d_cap num] > 0} { 
        set x_cap [vecscale .1 [$d_cap get x]]
        set y_cap [vecscale .1 [$d_cap get y]]
        set z_cap [vecscale .1 [$d_cap get z]]
        
        puts $Bfile $x_cap
        puts $Bfile $y_cap
        puts $Bfile $z_cap
    } else {
        puts $Bfile ""
        puts $Bfile ""
        puts $Bfile ""
    } 
}

close $Pfile
close $Bfile
