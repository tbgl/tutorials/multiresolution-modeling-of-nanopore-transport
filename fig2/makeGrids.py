from pdb import set_trace
from parseComsol import parseComsolData, parseComsolData3D
import numpy as np
from scipy.interpolate import griddata
# from gridDataFormats import Grid 
from gridData import Grid
import dill as pickle
from writeDx import writeDx
from skimage.segmentation import find_boundaries
from scipy import signal
import sys
## Gaussian blur
def blur(g,delta,width,mode='same'):
    """Apply a gaussian blur to a gridData grid"""
    width = float(width)
    
    assert( np.size(delta) == 3 )
    kernelSize = [int(np.ceil(width*5.0/i)) for i in delta]
    for a in kernelSize:
        assert( a > 3 )

    ## Construct convolution kernel
    i,j,k = [np.arange(int(a)) for a in kernelSize] # get indeces
    i,j,k = np.meshgrid(i,j,k,indexing='ij')   # turn indeces into meshGrid indeces
    gauss = [signal.gaussian( a+1, width/x )
             for a,x in zip(kernelSize,delta)]
    print("gauss:", gauss)
    kernel = gauss[0][i]*gauss[1][j]*gauss[2][k]
    kernel = kernel/kernel.sum()
    # info( np.shape(g), np.shape(kernel) )
    return signal.fftconvolve(g, kernel, mode=mode)
                # edges=g.edges)

## Grow grid
def growMask(g,width):
    assert( type(g) is Grid )
    assert( np.size(g.delta) == 3 )
    kernelSize = [int(np.ceil(width*2.0/i)) for i in g.delta]
    for a in kernelSize:
        assert( a > 1 )

    ## Construct convolution kernel
    i,j,k = [np.arange(a) for a in kernelSize] # get indeces
    mg = np.meshgrid(i,j,k,indexing='ij') # turn indeces into meshGrid indeces
    
    r = np.array( [a-n*0.5 for a,n in zip(mg,kernelSize)] )
    kernel = np.sum(r**2,axis=0) <= width**2   # sphere

    # kernel = kernel/kernel.sum()
    # info( np.shape(g.grid), np.shape(kernel) )
    return Grid(signal.fftconvolve(g.grid, kernel, mode='same') >= 1,
                origin=g.origin, delta=g.delta)
                # edges=g.edges)
                

## Pad Grid with padCells (either 1 number of 3-value tuple) containing padValue
def padGrid(grid, padCells=1, padValue=0):
    # info(np.shape(grid.grid))    
    if type(padCells) is int:
        padCells = (padCells,padCells,padCells)
    assert( len(padCells) == 3)
    padCells = np.array(padCells)

    g = np.ones([a+b for a,b in zip(np.shape(grid.grid),padCells*2)])
    g = g*padValue
    s = [slice(a,-a) for a in padCells]
    g[s[0],s[1],s[2]] = grid.grid

    o = grid.origin - padCells * grid.delta
    
    return Grid(grid=g, origin=o, delta=grid.delta)  

# ## Flatten an array of numpy arrays
# def flattenStack(*arrs):
#     return np.hstack([np.reshape(a,-1) for a in arrs])

# if __name__ == "main":
#     dataFile = '/home/cmaffeo2/Downloads/test-voltage.txt'
#     print("Reading",dataFile)
#     r,z,fields = parseComsolData(dataFile)

#     print("Creating meshgrids")
#     R0,Z0 = np.meshgrid(r,z, indexing='ij')

#     print("Creating meshgrids B")
#     # dx = 0.3333333
#     dx = 1.0
#     x = y = np.arange(-r[-1],r[-1],dx)
#     X,Y,Z = np.meshgrid(x,y,z, indexing='ij')
#     print("Creating meshgrids C")
#     R = np.sqrt(X**2 + Y**2)
#     print("Creating meshgrids D")

#     # print(np.shape(R0),np.shape(Z0))
#     rz = np.array([(b,a) for a,b in zip(R0.flatten(),Z0.flatten())])
#     print(np.shape(rz), np.shape(fields[0].flatten()) )


#     print("Creating meshgrids E")

#     newFields = []
#     for f in fields:
#         print("Gridding data", len(newFields))
#         newFields.append( 
#             griddata( rz, f.flatten(), (R,Z), method='linear' ) )


#     print("Creating gridData grid")
#     g = Grid( newFields[-1], 
#               origin = (np.min(X),np.min(Y),np.min(Z)),
#               delta = ( dx, dx, np.mean(np.diff(z)) ) )

#     g.export("test.dx")

def replaceNaNWithWall(pot1, NaNvalues):
    pot=pot1
    for i in range(1,11):
        NaNTmp = np.zeros(np.shape(NaNvalues))
        NaNTmp[np.where(np.isnan(NaNvalues))] = -1
        tmp=find_boundaries(NaNTmp, mode='thick')
        print(len(tmp))
        idx = np.nonzero(tmp)
        NaNvalues[idx] = 0
        pot[idx] = 100*i*i
    return pot

# for mV in (100,150,200,250,300,350,400):
for mV in (600,):
    #dataFile = 'pot-%03d.txt' % mV
    #out =      'pot-%03d.dx'  % mV
    # dataFile = 'PressurePipe'
    dataFile = 'fig2_COMSOL.txt'
    E_out  = 'electric.dx'
    Barrier_out = 'confine.dx' 
    print("Reading",dataFile)
    comsolData = parseComsolData3D(dataFile)
    # with open(pickleFile,"w") as fh:
    #     pickle.dump(comsolData, fh)
    x,y,z,fields = comsolData
    dx,dy,dz = [a[1]-a[0] for a in (x,y,z)]
    X,Y,Z = np.meshgrid(x,y,z, indexing='ij')

    #print("Creating gridData grid", out)
    #print(len(fields[3]))
    #break
    #pot = -1 * fields[0] * 23.06055 * mV / 600.0
    pot = fields[0]
    electric = 23.045131 * fields[0]

    def replaceNanWithLocalAvg(pot):
        for i,j,k in np.transpose(np.where(np.isnan(pot))):
            # print(ijk)
            # i,j,k = ijk
            # print(i,j,k)
            pot[i,j,k] = np.nanmean( pot[i-1:i+1,j-1:j+1,k-1:k+1] )
        assert( np.any(np.isnan(pot)) == False )
        return pot

    #NaNs = np.zeros( np.shape(pot) )
    # barrier[np.where( np.abs(fields[3]) < 1e-1 )] = 10
        
    pot[np.where(~np.isnan(fields[0]))] = 0
    barrier = np.zeros(np.shape(pot))
    barrier[np.where(np.isnan(pot))] = 20000
    
    barrier=replaceNaNWithWall(barrier, pot)
    #barrier[np.where( np.isnan(fields[0]) ) ] = 3000

    #pot[np.where(np.isnan(pot))] = 20
    electric[np.where(np.isnan(electric))] = 0
    #pressure = replaceNanWithLocalAvg(pressure)

    # if np.any(np.isnan(pot)):
    #     print("Pot includes %d nans" % np.sum(np.isnan(pot)))
    #     pot = replaceNanWithLocalAvg(pot)


    delta = np.array( (dx,dy,dz) )
    origin = np.array( (np.min(X),np.min(Y),np.min(Z)) )

    sh0 = np.array(np.shape(pot))
    print(delta)

    # pot = blur( barrier+pot, delta, width=1, mode='valid' )
    #pot = blur( pot, delta, width=1, mode='valid' )
    #vx = blur(vx, delta, width=1, mode='valid')
    #vy = blur(vy, delta, width=1, mode='valid')
    #vz = blur(vz, delta, width=1, mode='valid')
    #pressure = blur(pressure, delta, width=1, mode='valid')

    sh1 = np.array(np.shape(pot))
    #sh1 = np.array(np.shape())
    print(sh0,sh1)
    origin = origin + delta * ((sh0-sh1)/2)
    ## Subtract value at boundary
    # pot = pot - np.mean( pot[Z == np.min(Z)] )
    #print( "Mean at boundary", np.mean( pot[:,:,0] ) )

    #writeDx(out, pot,
    #        origin = origin*10,
    #        delta = delta*10,
    #        fmt = '%.6f' )
    #vx = np.reshape(vx,(50, 50, 50),order='C')
    #vy = np.reshape(vy,(50, 50, 50), order='C')
    #vz = np.reshape(vz,(150*50*50,1,1), order='C')
    #pressure = np.reshape(pressure, (50, 50, 50), order='C')
    #barrier = np.reshape(barrier, (50, 50, 50), order='C')

    #g = Grid(vz, origin=origin*10, delta=delta*10)
    #g.export(Vz_out)
    writeDx( E_out, electric, origin = origin*10, delta = delta*10, fmt= '%.15f')
    writeDx(Barrier_out, barrier, origin = origin*10, delta = delta*10, fmt= '%.8f')
   
    #writeDx(Barrier_out, pot, origin = origin*10, delta = delta*10, fmt= '%.8f')
