from mrdna import DoubleStrandedSegment, SegmentModel
from mrdna.coords import readArbdCoords
import numpy as np

for run_num in range(1):
    
    # Set up the model in the box.
###########################    
    shift = 10*np.array((160/2,0,.25))
    #shift = 10*np.array((160,0,.25))
    seg1 = DoubleStrandedSegment(
        name="helix1",
        start_position = np.array((0,-550,-800))+shift, 
        end_position = np.array((0,550,800))+shift, 
        #start_position = np.array((0,-550,0))+shift, 
        #end_position = np.array((0,550,0))+shift, 
##########################
        num_bp = 5000)
    
    model = SegmentModel( [seg1],
        local_twist = False,
        max_basepairs_per_bead = 100,
        max_nucleotides_per_bead = 100,
        dimensions=(20000,5000,5000))
    model.grid_potentials = [] # clear any existing potential

    model.add_grid_potential("confine.dx", scale=0.5, per_nucleotide = True)
        # This line shouldn't be necessary but add_grid_potentials doesn't actually add the thing automatically so this is a temporary hack

    steps1 = 100e6
    steps2 = 10e6
    steps3 = 10e6
    steps4 = 10e6
    steps5 = 2e6
    steps6 = 8e9
    tstep1 = 2e-4
    tstep2 = 2e-4
    tstep3 = 2e-4
    tstep4 = 1e-4
    tstep5 = 40e-6
    tstep6 = tstep5
    n1 = 100
    n2 = 50
    n3 = 25
    n4 = 5
    n5 = 5000/4999
    n6 = n5
    # Run the first coarse simulation, update the model spline with simulation results

    model.generate_bead_model(
        max_basepairs_per_bead = n1,
        max_nucleotides_per_bead = n1, 
        local_twist=False,
        escapable_twist=False)
    model.simulate(
        gpu = 1,
        output_name = "loop1", 
        directory='output{:02d}' .format(run_num),
        num_steps=steps1, 
        output_period=1e4, 
        timestep=tstep1)
    
    coords = readArbdCoords('output{:02d}/output/loop1.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the second finer simulation, update the coordinates again
    
    model.generate_bead_model(
        max_basepairs_per_bead = n2,
        max_nucleotides_per_bead = n2, 
        local_twist=False,
        escapable_twist=False)
    model.simulate(
        gpu = 1,
        output_name = "loop2",
        directory='output{:02d}' .format(run_num),
        num_steps=steps2, 
        output_period=1e4, 
        timestep=tstep2)
    coords = readArbdCoords('output{:02d}/output/loop2.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the third finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n3,
        max_nucleotides_per_bead = n3, 
        local_twist=False,
        escapable_twist=False)
    model.simulate(output_name = "loop3",
        gpu = 1,
        directory='output{:02d}' .format(run_num),
        num_steps=steps3,
        output_period=1e4,
        timestep=tstep3)
    coords = readArbdCoords('output{:02d}/output/loop3.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the fourth finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n4,
        max_nucleotides_per_bead = n4,
        local_twist=False, 
        escapable_twist=False)
    model.simulate(output_name = "loop4",
        gpu = 1,
        directory='output{:02d}' .format(run_num),
        num_steps=steps4,
        output_period=1e4,
        timestep=tstep4)
    coords = readArbdCoords('output{:02d}/output/loop4.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the fifth finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n5,
        max_nucleotides_per_bead = n5, 
        local_twist=True, 
        escapable_twist=False)
    model.simulate(output_name = "loop5",
        gpu = 1,
        directory='output{:02d}' .format(run_num),
        num_steps=steps5,
        output_period=1e4,
        timestep=tstep5)
    coords = readArbdCoords('output{:02d}/output/loop5.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    accelerate = 1
    model.add_grid_potential("electric.dx", scale=-0.25*accelerate, per_nucleotide = True)
    
    model.generate_bead_model(
        max_basepairs_per_bead = n6,
        max_nucleotides_per_bead = n6, 
        local_twist=True, 
        escapable_twist=False)

    model.simulate(output_name = "run",
        gpu = 1,
        directory='output{:02d}' .format(run_num),
        num_steps=steps6,
        output_period=1e4,
        timestep=tstep6)
    

#    seg  = model.segments[0]
#    arcpos = np.linspace(0,1,seg.num_nt)
#    pos1 = seg.contour_to_position(arcpos)
#    pos2 = pos1 + np.array((5,0,0))
#    pos  =  np.empty((pos1.shape[0]+pos2.shape[0],3))
#    pos[0::2] = pos1# + shift
#    pos[1::2] = pos2# + shift
#
#    stuff=np.loadtxt("output{:02d}/loop5.particles.txt".format(run_num),dtype='U4, i4, U4, f8,f8,f8')
#    for i in range(len(stuff)):
#        stuff[i][3] = pos[i,0]
#        stuff[i][4] = pos[i,1]
#        stuff[i][5] = pos[i,2]
#    
#    np.savetxt("output{:02d}/output/ds5000.coords.txt".format(run_num),stuff,fmt='%s %i %s %f8 %f8 %f8')
#    print("Finished run {:02d}" .format(run_num))
