proc seq { s f {inc 1}} {
    # builds s..f
    if { $inc <= 0 } { error "seq: inc (third arg) must be positive!"; return }
    if { $s > $f } { set inc [expr -1*$inc] }
    for { } { ($f-$s)*$inc >= 0 } { set s [expr $s+$inc] } { lappend ret $s }
    return "$ret"
    }



#cd ../../COMSOL 
#source slit.exp.tcl
#cd ../frontera/analysis/


#set volt 03
#set sim 02

#cd ../${volt}V/

mol new ../run.psf
set dcds [lsort [glob -type f run*dcd]]

foreach file $dcds {
    mol addfile $file waitfor all 
}

set max [molinfo top get numframes]
set f $max
set num_first 0
set num_slit 0
while {$f > 0 && $num_slit == 0} {
    set f [expr $f-1]
    set num_slit [[atomselect top "(type D001 or type D000) and x > 1600 and x < 5600" frame $f] num]
}
set finish [expr $f+1]
while {$f > 0 && $num_first != 5000} {
    set f [expr $f-1]
    set num_first [[atomselect top "(type D001 or type D000) and x < 1600" frame $f] num]
}
set start $f

puts "$start\t$finish"

set step 700
set first [expr $start]
set last [expr $finish+$step]

set frames [seq $first $last $step]

set condfile [open "dnaloc.txt" w]
foreach ff $frames {
    puts $ff
    set ffstring [format "%05d" $ff]
    set d_slit [atomselect top "(type D001 or type D000) and x > 1600 and x < 5600" frame $ff]
    
    if {[$d_slit num] > 0} { 

        set x_slit [vecscale .1 [$d_slit get x]] 
        set y_slit [vecscale .1 [$d_slit get y]] 
        set z_slit [vecscale .1 [$d_slit get z]] 

        puts $condfile $x_slit
        puts $condfile $y_slit
        puts $condfile $z_slit

    } 
}

close $condfile
