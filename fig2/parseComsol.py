# -*- coding: utf-8 -*-
from pdb import set_trace
import numpy as np
import glob,re,sys
import dill as pickle

def parseComsolData(filename):
    
    with open(filename) as fh:
        for line in fh:
            if re.match('^%',line): 
                continue
            else:
                break
        r = np.array([float(x) for x in line.split()])

        for line in fh:
            if re.match('^%',line): 
                continue
            else:
                break
        z = np.array([float(x) for x in line.split()])

        data = np.loadtxt(fh, comments='%')

    shape = np.shape(data)
    numFields = shape[0]//len(z)

    # print(len(r))
    # print(len(z))
    # print(np.shape(data))
    data = np.reshape(data, (numFields,len(z),len(r)))

    retData = []
    for i in range(numFields):
        retData.append(data[i,:,:].T)
    return r, z, np.array(retData)

def parseComsolData3D(filename):
    with open(filename) as fh:
        for line in fh:
            if re.match('^%',line): 
                continue
            else:
                break
        x = np.array([float(a) for a in line.split()])

        for line in fh:
            if re.match('^%',line): 
                continue
            else:
                break
        y = np.array([float(a) for a in line.split()])

        for line in fh:
            if re.match('^%',line): 
                continue
            else:
                break
        z = np.array([float(a) for a in line.split()])

        def readBlock(fh,numLines):
            i=0
            line = None
            for line in fh:
                if len(line) > 0 and line[0] == '%':
                    continue
                if i > 1e6:
                    raise Exception("Too many iterations")
                    
                i+=1
                if i == numLines: break
                yield line
            if line is not None:
                yield line

        # blockLen = len(z)
        blockLen = len(y)

        # data = []
        # while True:
        #     block = np.loadtxt(readBlock(fh,blockLen)) # [ len(z) , N_per_line ]
        #     # print(np.shape(block))
        #     if len(block) < blockLen:
        #         break
        #     # data.append( block.T ) # untested!
        #     data.append( block )
        def readFile(fh):
            for line in fh:
                if len(line) > 0 and line[0] == '%': continue
                yield line
            
        data = np.loadtxt(readFile(fh))

    data = np.array(data)       # [ N_total / ( len(z) * N_per_line ) ,  len(z) , N_per_line ]
    shape = np.shape(data)
    
    print("len(xyz):", len(x), len(y), len(z)) #shape: (2005, 751, 401)
    print("shape:",shape)
    numFields = shape[0]*shape[1] // ( len(x)*len(y)*len(z) )
    print("number of fields: "+str(shape[0]))
    fieldStride =  shape[0] // numFields
    # set_trace()

    retData = []
    for i in range(numFields):
        tmp = data[ i*fieldStride:(i+1)*fieldStride, :]
        #retData.append( np.reshape(tmp, (len(x),len(y),len(z)), order='F') ) # bad
        #retData.append( np.reshape(tmp, (len(x),len(y),len(z)), order='C') )
        # set_trace()
        retData.append( np.reshape(tmp, (len(x),len(y),len(z)), order='C') )
        #retData.append( np.reshape(tmp.T, (len(x),len(y),len(z)), order='F') )
    # print(len(x),len(y),len(z))
    # print(np.shape(data))
    
    # data = np.reshape(data, (numFields,len(y),len(z),len(x)))
    # reshape, transpose ? 
    ## Data claims to order x fast
    # data = np.reshape(data, (numFields,len(z),len(y),len(x)))
    # retData = data
    # retData = []
    # for i in range(numFields):
    #     retData.append(data[i,...].T)
    return x,y,z, np.array(retData)


# dataFile = '/home/cmaffeo2/Downloads/test-voltage.txt'
# r,z,fields = parseComsolData(dataFile)
# set_trace()
