from mrdna import SingleStrandedSegment, DoubleStrandedSegment, SegmentModel
from mrdna.arbdmodel.coords import readArbdCoords
import numpy as np

def makeArm(segMain, pos, loc, l): 
    seg = DoubleStrandedSegment(
        name="arm_"+str(loc),
        start_position = pos + np.array([1,0,0]),
        end_position   = pos + np.array([l*3.4 + 1,0,0]),
        num_bp = l)
    segMain.add_crossover(nt=(loc), other=seg, other_nt=0, strands_fwd=(True,True))
    seg.add_crossover(nt=0 , other=segMain, other_nt=(loc)+1, strands_fwd=(False,True))
    
    return [seg]

steps1 =  300e6
#steps2 = 300e6
steps3 =  300e6
steps4 =  60e6
steps5 =  60e6
steps6 =  1e10
tstep1 = 2e-4
#tstep2 = 2e-4
tstep3 = 2e-4
tstep4 = 1e-4
tstep5 = 40e-6
tstep6 = tstep5
n1 = 100
#n2 = 50
n3 = 25
n4 = 5
n5 = 5000/4999
n6 = n5

segs = []

starts = [] 
offset = 200

## This section generates the locations of the attachments for dumbbell pairs
#################
l = 14
d = 4
s = 58

nums = [22]
#ds = [6,4,2,0,-2]
ds = [16]
jump = s
pos = offset

for i in range(len(nums)):
    for j in range(nums[i]):
        starts.append(pos)
        pos += l + ds[i]
    pos += jump - l - ds[i]

#num_barages = 4
#len_barages = 6
#gap = 18
#for i in range(num_barages):
#    for j in range(len_barages):
#        starts.append(pos)
#        pos += gap
#    pos += s - gap
print(starts)

#for run_num in range(1,5):
for run_num in range(1):
    dim = 5000 
    nbp = starts[-1]+offset
    # Set up the model in the box.
##########################    
    eq1_spring = np.array((0,0,-250)) 
    eq3_spring = np.array((0,0,-125)) 
    eq4_spring = np.array((0,0,0)) 
    eq5_spring = np.array((0,0,125)) 
    eq_spring = np.array((0,0,30)) 
    segMain = DoubleStrandedSegment(
        name="helix1",
        start_position = eq1_spring, 
        end_position = np.array((0,0,-0.4*nbp)), 
##########################
        num_bp = nbp)

    model = SegmentModel( [segMain],
        local_twist = False,
        max_basepairs_per_bead = n1,
        max_nucleotides_per_bead = n1,
        dimensions=(dim,dim,dim))
    model.grid_potentials = [] # clear any existing potential

    model.add_grid_potential("confine.dx", scale=1.0, per_nucleotide = True)
        # This line shouldn't be necessary but add_grid_potentials doesn't actually add the thing automatically so this is a temporary hack
    
    
    # Run the first coarse simulation, update the model spline with simulation results

    model.generate_bead_model(
        max_basepairs_per_bead = n1,
        max_nucleotides_per_bead = n1, 
        local_twist=False,
        escapable_twist=False)
    model.segments[0].beads[0].add_restraint((1.0,eq1_spring))
    model.simulate(
        output_name = "loop1", 
        directory='output{:02d}' .format(run_num),
        num_steps=steps1, 
        output_period=1e4, 
        gpu = 1,
        timestep=tstep1)
    coords = readArbdCoords('output{:02d}/output/loop1.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    

#    # Run the second finer simulation, update the coordinates again
#    
#    model.generate_bead_model(
#        max_basepairs_per_bead = n2,
#        max_nucleotides_per_bead = n2, 
#        local_twist=False,
#        escapable_twist=False)
#    model.segments[0].beads[0].add_restraint((1.0,eq_spring))
#    model.simulate(
#        output_name = "loop2",
#        directory='output{:02d}' .format(run_num),
#        num_steps=steps2, 
#        output_period=1e4, 
#        timestep=tstep2)
#    coords = readArbdCoords('output{:02d}/output/loop2.restart' .format(run_num))
#    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the third finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n3,
        max_nucleotides_per_bead = n3, 
        local_twist=False,
        escapable_twist=False)
    model.segments[0].beads[0].add_restraint((1.0,eq3_spring))
    model.simulate(output_name = "loop3",
        directory='output{:02d}' .format(run_num),
        num_steps=steps3,
        output_period=1e4,
        gpu = 1,
        timestep=tstep3)
    coords = readArbdCoords('output{:02d}/output/loop3.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the fourth finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n4,
        max_nucleotides_per_bead = n4,
        local_twist=False, 
        escapable_twist=False)
    model.segments[0].beads[0].add_restraint((1.0,eq4_spring))
    model.simulate(output_name = "loop4",
        directory='output{:02d}' .format(run_num),
        num_steps=steps4,
        output_period=1e4,
        gpu = 1,
        timestep=tstep4)
    coords = readArbdCoords('output{:02d}/output/loop4.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the fifth finer simulation, update the coordinates again
    
    segMain = DoubleStrandedSegment(
        name="main",
        start_position = np.array((0,dim/4-500,0)), 
        end_position = np.array((0,-dim/4+500,0)), 
##########################
        num_bp = nbp)

#########
# Now we attach the dumbbells we want at the locations specified above.

    sfrac = np.array(starts)/nbp
    print(starts)
 
    main = model.segments[0]
    pos_start = main.contour_to_position(sfrac)
    j=0
    for i in range(len(starts)):
        segs = segs + makeArm(segMain,pos_start[j],starts[j],l)
        j+=1

#########
# Now we set up and run the final model

    final = SegmentModel([segMain]+segs,
        local_twist = True,
        max_basepairs_per_bead = n5,
        max_nucleotides_per_bead = n5,
        dimensions=(dim,dim,dim))

    print(final.segments)    

    final.grid_potentials = []
    final.add_grid_potential("confine.dx", scale=1.0, per_nucleotide = True)

    #final.update_splines(coords)
    final.segments[0].position_spline_params = main.position_spline_params
    # if previous sim had twist
    #final.segments[0].quaternion_spline_params = main.quaternion_spline_params

    final.generate_bead_model(
        max_basepairs_per_bead = n5,
        max_nucleotides_per_bead = n5, 
        local_twist=True, 
        escapable_twist=False,
        one_bead_per_monomer = True)

    final.segments[0].beads[0].add_restraint((1.0,eq5_spring))

    final.simulate(output_name = "loop5",
        directory='output{:02d}' .format(run_num),
        num_steps=steps5,
        output_period=1e4,
        gpu = 1,
        timestep=tstep5)
    coords = readArbdCoords('output{:02d}/output/loop5.restart' .format(run_num))
    final.update_splines(coords) # Fits a spline through the coordinates

    # Run the sixth finer simulation, update the coordinates again

    accelerate = 1
    final.add_grid_potential("electric.dx", scale=1.0*accelerate, per_nucleotide = True)    

    final.generate_bead_model(max_basepairs_per_bead = n6,
        max_nucleotides_per_bead = n6,
        local_twist=True, 
        escapable_twist=False,
        one_bead_per_monomer = True)
    final.simulate(output_name = "run_in",
        dry_run=True,
        directory='output{:02d}' .format(run_num),
        num_steps=steps6,
        output_period=1e4,
        gpu = 1,
        timestep=tstep6)
    
    final.grid_potentials = []
    final.add_grid_potential("confine.r.dx", scale=1.0, per_nucleotide = True)    
    final.add_grid_potential("electric.r.dx", scale=1.0*accelerate, per_nucleotide = True)    

    final.generate_bead_model(max_basepairs_per_bead = n6,
        max_nucleotides_per_bead = n6,
        local_twist=True, 
        escapable_twist=False,
        one_bead_per_monomer = True)
    final.simulate(output_name = "run_out",
        dry_run=True,
        directory='output{:02d}' .format(run_num),
        num_steps=steps6,
        output_period=1e4,
        gpu = 1,
        timestep=tstep6)
    
#    seg  = model.segments[0]
#    arcpos = np.linspace(0,1,seg.num_nt)
#    pos1 = seg.contour_to_position(arcpos)
#    pos2 = pos1 + np.array((5,0,0))
#    shift = 10*np.array((160/2,0,.25))
#    pos  =  np.empty((pos1.shape[0]+pos2.shape[0],3))
#    pos[0::2] = pos1 + shift
#    pos[1::2] = pos2 + shift
#    np.savetxt("output{:02d}/output/ds5000.coords.txt" .format(run_num),pos)
#    print("Finished run {:02d}" .format(run_num))
#
##model.segments[-1].beads[-1].add_restraint((1.0,eq_spring))
#def restraint_function(model):
#    #model.segments[-1].beads[-1].add_restraint((1.0,eq_spring))
#
#model._generate_bead_callbacks.append(restraint_function)
#
#contour = seg.nt_pos_to_contour(49)
#bead = seg.get_nearest_bead(counter)
#bead.add_restraint()
