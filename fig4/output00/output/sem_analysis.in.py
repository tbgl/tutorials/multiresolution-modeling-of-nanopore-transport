import numpy as np
from fenics import *
from mshr import *
from scipy.interpolate import RegularGridInterpolator, interp1d
import time
import sys
import glob
#from matplotlib import pyplot as plt

start = time.time()

outputPeriod = 10000
stepsize = 40e-6
step = 8

voltage = 0.4
salts = [1]

info = {
    0: "Low Salt, a mixture of values for different ion specifies.",
    1: "High Salt, just copying a 2M KCl graph and scaling it for 4M KCl"
}

rs = {
    0: np.linspace(0,26,1000)/10, 
    1: np.linspace(0,25,1000)/10
}

# the conductivity at any point in the vicinity of DNA is given by the sum over ion types of...
# the product of the charge, local number density, local mobility, local 

# arrays have positive ion values first and negative second
qs= {
     0: (1,1),
     1: None
}
# 0: bulk mobility values from BELK2016 s5c
# 0: normalized mobility curves taken from Figure from BELK2016 3d, 2M KCl, K+ at T=295K
bulk_mobilities = {
    0: (55e-5,57e-5),
    1: None
}

mobilities = {
    0: (interp1d(
np.concatenate([[0],np.arange(0.5,27,1)])/10,
bulk_mobilities[0][0]*np.concatenate([[0,0,0,0,0],np.array([.01,.05,.09,.12,.23,.36,.61,.75,.83,.95,1,1,1,1,1,1,1,1,1,1,1,1,1])]),
    kind='cubic')
    ,
    interp1d(
np.concatenate([[0],np.arange(0.5,27,1)])/10,
bulk_mobilities[0][1]*np.concatenate([[0,0,0,0,0],np.array([.01,.05,.09,.12,.23,.36,.61,.75,.83,.95,1,1,1,1,1,1,1,1,1,1,1,1,1])]),
    kind='cubic')),
    1: None
}
# 0: From Chris's 2010 PRL, SI 10c, 170mM Na+ for the positive ions, and 170mM Cl- for the negative ions
center=10.5/10
slope=8/10
saturated=1.25e-4
func = saturated*.5*(1+np.tanh((rs[0]-center)/slope))
func=func[-1]/(func[-1]-func[0])*(func-func[0])
num_densities = {
    0: (interp1d(np.concatenate([[0],np.arange(0.3,30,2)])/10, np.array([0, 0,1.3,5,4.9,3.75,6.3,7.5,5,3,2,1.8,1.6,1.5,1.35,1.25])*1e-4, kind='cubic')
        , interp1d(rs[0],func,kind='cubic')),
    1: None
}

BULK4M = 17.3 # S/m bulk conductivity of 4M LiCl
conductivities = {
    0: (
        interp1d(rs[0],num_densities[0][0](rs[0])*mobilities[0][0](rs[0]),kind='cubic'),
        interp1d(rs[0],num_densities[0][1](rs[0])*mobilities[0][1](rs[0]),kind='cubic')
        ),
    1: (
        interp1d(np.concatenate([np.linspace(0,.4,9),np.linspace(.55,2.55,21)]),
        BULK4M/(.29+.35)*np.array([0,0,0,0,0,0,0,0,0,
                  0.035,.05,.04,.045,.07,.115,.2,.305, .36,.35,.33,.33,.315,.3,.305,.3,.295,.295,.285,.29,.29]),kind='cubic'),
        interp1d(np.concatenate([np.linspace(0,.4,9),np.linspace(.55,2.55,21)]),
        BULK4M/(.29+.35)*np.array([0,0,0,0,0,0,0,0,0,
                  0, 0,.005,.01 ,.02,.035,.06,.1,.16,.215,.265, .295, .31,.32, .325, .34, .35,.34, .35, .35,.35]),kind='cubic')
        )
}

def combine(arrs,dist):
    return arrs[0](dist)+arrs[1](dist)


def loadFunc(mesh, F, sig, interpfunction):
    vec = sig.vector()
    values = vec.get_local()
    
    dofmap = F.dofmap()
    my_first, my_last = dofmap.ownership_range()
    
    n = F.dim()
    d = mesh.geometry().dim()
    F_dof_coordinates = F.tabulate_dof_coordinates()
    F_dof_coordinates.resize((n,d))
    
    unowned = dofmap.local_to_global_unowned()
    dofs = filter(lambda dof: dofmap.local_to_global_index(dof) not in unowned,
                range(my_last-my_first))
    coords = F_dof_coordinates[list(dofs)]
    values[:] = interpfunction(coords)
    vec.set_local(values)
    vec.apply('insert')

def compute_gridcond(cutoff,initials,finals,Dx,Dy,Dz,x,y,z,dna,cond_base,conductivities,salt,BULK):
    INFTY = 10000
    
    if len(dna[0])>0:
        dnax = dna[0]
        dnay = dna[1]
        dnaz = dna[2]
    
        dist = INFTY * cond_base
        
        x_init, y_init, z_init = initials[0], initials[1], initials[2]
        x_fin, y_fin, z_fin = finals[0], finals[1], finals[2]
        
        xi = np.maximum(np.around((dnax-cutoff)/Dx)*Dx,x_init*np.ones_like(dnax))
        xi = np.where( np.abs(xi-dnax) < cutoff , np.maximum(xi-Dx,x_init*np.ones_like(dnax)), xi)
        xi_index=np.around((xi-x_init)/Dx).astype(int)
        xf = np.minimum(np.around((dnax+cutoff)/Dx)*Dx,x_fin*np.ones_like(dnax))
        xf = np.where( np.abs(xf-dnax) < cutoff , np.minimum(xf+Dx,x_fin*np.ones_like(dnax)), xf)
        # The plus one makes sure the indices include the final one.
        xf_index=np.around((xf-x_init)/Dx).astype(int)+1
        
        yi = np.maximum(np.around((dnay-cutoff)/Dy)*Dy,y_init*np.ones_like(dnay))
        yi = np.where( np.abs(yi-dnay) < cutoff , np.maximum(yi-Dy,y_init*np.ones_like(dnay)), yi)
        yi_index=np.around((yi-y_init)/Dy).astype(int)
        yf = np.minimum(np.around((dnay+cutoff)/Dy)*Dy,y_fin*np.ones_like(dnay))
        yf = np.where( np.abs(yf-dnay) < cutoff , np.minimum(yf+Dy,y_fin*np.ones_like(dnay)), yf)
        yf_index=np.around((yf-y_init)/Dy).astype(int)+1

        zi = np.maximum(np.around((dnaz-cutoff)/Dz)*Dz,z_init*np.ones_like(dnaz))
        zi = np.where( np.abs(zi-dnaz) < cutoff , np.maximum(zi-Dz,z_init*np.ones_like(dnaz)), zi)
        zi_index=np.around((zi-z_init)/Dz).astype(int)
        zf = np.minimum(np.around((dnaz+cutoff)/Dz)*Dz,z_fin*np.ones_like(dnaz))
        zf = np.where( np.abs(zf-dnaz) < cutoff , np.minimum(zf+Dz,z_fin*np.ones_like(dnaz)), zf)
        zf_index=np.around((zf-z_init)/Dz).astype(int)+1
        
        for i in range(0, len(dnax)):
            positions = np.array([[[[xx,yy,zz] for zz in (z[zi_index[i]:zf_index[i]])] for yy in (y[yi_index[i]:yf_index[i]])] for xx in (x[xi_index[i]:xf_index[i]])])
            distances = np.sqrt(np.sum(np.square(positions-dna.T[i]),axis=3))
            dist_section = dist[xi_index[i]:xf_index[i],yi_index[i]:yf_index[i],zi_index[i]:zf_index[i]]
            # the second condition does two things. First, it ensures that the walls where conductivity is zero remain as such. It does this because dist was first defined to be zero outside of the capillary, and so any value of d calculated above is guaranteed to not be less than zero. Therefore, the points outside the capillary always have a value of zero in the dist array. Secondly, it only rewrites the distance array if the presently calculated distance d is less than any previously calculated distance. In this way, it takes the minimum value of distance to the DNA beads.
            dist[xi_index[i]:xf_index[i],yi_index[i]:yf_index[i],zi_index[i]:zf_index[i]] = np.where((distances < cutoff) & (distances < dist_section),distances,dist_section)

# This is NOT creating an ndarray independent of dist, it's merely creating a new label that points to the same array dist. change cond, and you will also change dist.
        cond = dist
        cond[(cond < INFTY-1) & (cond > 0)] = combine(conductivities[salt],cond[(cond < INFTY-1) & (cond > 0)])
        cond[cond > INFTY-1] = BULK
        return cond
    return BULK * cond_base

def condval(x,y,z,angles,zs,rs):
    r = np.sqrt(x**2+y**2)

    a,b = angles
    z0, z1, z2 = zs
    r0, r1, r2 = rs
    
    cond_base=np.zeros_like(x)
    cond_base[np.where(r <= r0)] = 1 
    cond_base[np.where( (r > r0) & (r <= r1) & (z >= z0+(r-r0)*(z1-z0)/(r1-r0)))] = 1 
    cond_base[np.where((r > r1) & (r <= r2) & (z >= z1+(r-r1)*(z2-z1)/(r2-r1)) )] = 1 
    
    return cond_base

def xyzrange(min_,max_,delta):
    return np.arange(min_-delta/2,max_+delta/2,delta)





###### Prepare conductivity Arrays
# arg1 is the voltage and arg2 is the simulation number

cutoff = 2.5
Dx, Dy, Dz = 1/2, 1/2, 1/2

angles = [.085,.021]
zs = [0,50,300 + cutoff + Dz]
r0 = 2 
rs = [r0, 
      r0 + zs[1]*np.tan(angles[0]), 
      r0 + zs[1]*np.tan(angles[0]) + (zs[2]-zs[1])*np.tan(angles[1])]
xy_dim = np.ceil(rs[2])
x = xyzrange(-xy_dim,xy_dim + Dx, Dx)
y = xyzrange(-xy_dim,xy_dim + Dy, Dy)
z = np.arange(zs[0], zs[2] + Dz, Dz)

X,Y,Z = np.meshgrid(x,y,z, indexing='ij')



initials = np.array([min(x),min(y),min(z)])
finals = np.array([max(x),max(y),max(z)])

numx = int((finals[0]-initials[0])/Dx)
numy = int((finals[1]-initials[1])/Dy)
numz = int((finals[2]-initials[2])/Dz)

solver = KrylovSolver("gmres", "amg")
#solver = KrylovSolver("gmres", "none")
#solver = LinearSolver("mumps")
parameters['krylov_solver']['nonzero_initial_guess'] = True
parameters["krylov_solver"]["monitor_convergence"] = True
#solver.parameters["linear_solver"] = "mumps"
#PETScOptions.set('ksp_rtol', '.05')
solver.parameters["relative_tolerance"] = 1e-8
solver.parameters["maximum_iterations"] = 20000
#solver.parameters["monitor_convergence"] = True
set_log_level(30)

tol = 1E-14

mesh = BoxMesh(Point(initials), Point(finals),numx,numy,numz)

ground = CompiledSubDomain('on_boundary && near(x[2],zwall,tol)',   tol=tol, zwall = initials[2])
terminal = CompiledSubDomain('on_boundary and near(x[2],zwall,tol)',tol=tol, zwall = finals[2])

boundary_parts = MeshFunction("size_t", mesh,mesh.topology().dim() - 1)
ground.mark(boundary_parts,1)
terminal.mark(boundary_parts,2)

ds = Measure('ds',domain=mesh, subdomain_data=boundary_parts)

#make Function Space
V = FunctionSpace(mesh, 'P', 1)
F = FunctionSpace(mesh, 'CG', 1)

bc_ground = DirichletBC(V, Constant(0), ground)
bc_terminal = DirichletBC(V, Constant(voltage), terminal)
bcs = [bc_ground, bc_terminal]

# define functions for variational problem
u = TrialFunction(V)
v = TestFunction(V)
sig = Function(F)
f= Constant(0)
DE = sig*dot(grad(u), grad(v))*dx - f*v*dx
a, L = lhs(DE), rhs(DE)

u1 = Function(V)

flux_in = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(1)
flux_out = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(2)

# voltt = sys.argv[1]
# numm = sys.argv[2]
# files = sorted(glob.glob("%s_%s_*" % (voltt, numm)))
# files = ['empty.txt','empty.txt']



#sim = '01'

#steppedframes = range(2667,5401)

cond_base = condval(X,Y,Z,angles,zs,rs)
for salt in salts:
    #BULK = 17.3 #4M S/m LiCl #1.660843 (10 S/m) #1M KCl when building in Angstroms
    BULK = combine(conductivities[salt],cutoff)
    counter = 0
    with open("{}_{}_{}_in.txt".format(2*r0,int(voltage*1000),step),"r") as fil:
        lines=fil.readlines()
        t = np.array(range(0,int(len(lines)/3)))*step*outputPeriod*stepsize/1000
    #    t = np.array(steppedframes)*step*outputPeriod*stepsize/1000
         
#        with open("{}_{}_{}_{}.current.txt".format(2*r0,int(voltage*1000),salt,step),"w") as fl:
#            pass
        DATA = []
        for frame in range(int(len(lines)/3)):
#        for frame in range(100,105):                
            cond_time1 = time.time()
    
            dna=np.array((lines[frame*3].split(),lines[frame*3+1].split(),lines[frame*3+2].split()),dtype=float)
            cond = compute_gridcond(cutoff,initials,finals,Dx,Dy,Dz,x,y,z,dna,cond_base,conductivities,salt,BULK)
            
            intsig = RegularGridInterpolator((x,y,z),cond,bounds_error=False,fill_value=BULK)
            
            cond_time2 = time.time()
            
            print('Time for cond_gen: ',cond_time2-cond_time1)
            
            ######### FEniCS section
            
            fenics_time1 = time.time()
            
            
            loadFunc(mesh,F,sig,intsig)
            
            A,bb = assemble_system(a,L,bcs)
            solver.set_operator(A)
            
            # Solve Problem
            
            solver.solve( u1.vector(),bb)
            
            fi = assemble(flux_in)
            fo = assemble(flux_out)
            print(fi-fo,fi,fo)
            
            fenics_time2 = time.time()
            
            print('Time for fenics: ',fenics_time2-fenics_time1)
            
            print('Total time: ', time.time() - start)
            
            DATA.append([fo, t[counter], fo-fi])
            #print(DATA)
#            with open("{}_{}_{}_{}.current.txt".format(2*r0,int(voltage*1000),salt,step),"a") as fl:
#                fl.write('{:e}\t{:e}\t{:e}\n'.format(fo, t[counter], fo-fi))

            counter+=1

        print(DATA)
        with open("{}_{}_{}_{}.in.current.txt".format(2*r0,int(voltage*1000),salt,step),"w") as fl:
            for row in DATA:
                fl.write('{:e}\t{:e}\t{:e}\n'.format(row[0],row[1],row[2]))
