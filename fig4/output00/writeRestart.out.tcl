#source ../../capi.procs.tcl

proc seq { s f {inc 1}} {
    # builds s..f
    if { $inc <= 0 } { error "seq: inc (third arg) must be positive!"; return }
    if { $s > $f } { set inc [expr -1*$inc] }
    for { } { ($f-$s)*$inc >= 0 } { set s [expr $s+$inc] } { lappend ret $s }
    return "$ret"
    }


proc typenum {ptypes name} {
    set var [lsearch $ptypes $name]
    return $var
}
# for the easy restart
#set frame 44334
# for the hard restart

set ptypes {D000 O000 S000}

foreach run [seq 0 0] {

    mol new run_in.psf
    mol addfile output/run_in.dcd waitfor all 
    
    set frame 0
    animate goto $frame
    while {[[atomselect top z<0] num] > 0} {
        set frame [expr $frame+1]
        animate goto $frame
    }
    
    set all [atomselect top "all"]
    
    set pos [$all get {x y z}]
    set indexes [$all get index]
    set types [$all get type]
    
    set zf [open out.restart.txt w]
    
    foreach index $indexes {
        set num [typenum $ptypes [lindex $types $index]]
        puts $zf "$num [lindex $pos $index]"
    }
    
    close $zf
}

exec /bin/sed -i "/inputParticle/s/.*/restartCoordinates out.restart.txt/" run_out.bd

