import numpy as np
from fenics import *
from mshr import *
from scipy.interpolate import RegularGridInterpolator, interp1d
import time
import sys
import glob
import pickle
from matplotlib import pyplot as plt

start = time.time()

outputPeriod = 10000
stepsize = 40e-6
step = 1000

voltage = .2
salts = [1]

info = {
    0: "Low Salt, a mixture of values for different ion specifies.",
    1: "High Salt, just copying a 2M KCl graph and scaling it for 4M KCl"
}

rs = {
    0: np.linspace(0,26,1000)/10, 
    1: np.linspace(0,25,1000)/10
}

# the conductivity at any point in the vicinity of DNA is given by the sum over ion types of...
# the product of the charge, local number density, local mobility, local 

# arrays have positive ion values first and negative second
qs= {
     0: (1,1),
     1: None
}
# 0: bulk mobility values from BELK2016 s5c
# 0: normalized mobility curves taken from Figure from BELK2016 3d, 2M KCl, K+ at T=295K
bulk_mobilities = {
    0: (55e-5,57e-5),
    1: None
}

mobilities = {
    0: (interp1d(
np.concatenate([[0],np.arange(0.5,27,1)])/10,
bulk_mobilities[0][0]*np.concatenate([[0,0,0,0,0],np.array([.01,.05,.09,.12,.23,.36,.61,.75,.83,.95,1,1,1,1,1,1,1,1,1,1,1,1,1])]),
    kind='cubic')
    ,
    interp1d(
np.concatenate([[0],np.arange(0.5,27,1)])/10,
bulk_mobilities[0][1]*np.concatenate([[0,0,0,0,0],np.array([.01,.05,.09,.12,.23,.36,.61,.75,.83,.95,1,1,1,1,1,1,1,1,1,1,1,1,1])]),
    kind='cubic')),
    1: None
}
# 0: From Chris's 2010 PRL, SI 10c, 170mM Na+ for the positive ions, and 170mM Cl- for the negative ions
center=10.5/10
slope=8/10
saturated=1.25e-4
func = saturated*.5*(1+np.tanh((rs[0]-center)/slope))
func=func[-1]/(func[-1]-func[0])*(func-func[0])
num_densities = {
    0: (interp1d(np.concatenate([[0],np.arange(0.3,30,2)])/10, np.array([0, 0,1.3,5,4.9,3.75,6.3,7.5,5,3,2,1.8,1.6,1.5,1.35,1.25])*1e-4, kind='cubic')
        , interp1d(rs[0],func,kind='cubic')),
    1: None
}

BULK4M = 17.3 # S/m bulk conductivity of 4M LiCl
conductivities = {
    0: (
        interp1d(rs[0],num_densities[0][0](rs[0])*mobilities[0][0](rs[0]),kind='cubic'),
        interp1d(rs[0],num_densities[0][1](rs[0])*mobilities[0][1](rs[0]),kind='cubic')
        ),
    1: (
        interp1d(np.concatenate([np.linspace(0,.4,9),np.linspace(.55,2.55,21)]),
        BULK4M/(.29+.35)*np.array([0,0,0,0,0,0,0,0,0,
                  0.035,.05,.04,.045,.07,.115,.2,.305, .36,.35,.33,.33,.315,.3,.305,.3,.295,.295,.285,.29,.29]),kind='cubic'),
        interp1d(np.concatenate([np.linspace(0,.4,9),np.linspace(.55,2.55,21)]),
        BULK4M/(.29+.35)*np.array([0,0,0,0,0,0,0,0,0,
                  0, 0,.005,.01 ,.02,.035,.06,.1,.16,.215,.265, .295, .31,.32, .325, .34, .35,.34, .35, .35,.35]),kind='cubic')
        )
}

def combine(arrs,dist):
    return arrs[0](dist)+arrs[1](dist)


def loadFunc(mesh, F, sig, interpfunction):
    vec = sig.vector()
    values = vec.get_local()
    
    dofmap = F.dofmap()
    my_first, my_last = dofmap.ownership_range()
    
    n = F.dim()
    d = mesh.geometry().dim()
    F_dof_coordinates = F.tabulate_dof_coordinates()
    F_dof_coordinates.resize((n,d))
    
    unowned = dofmap.local_to_global_unowned()
    dofs = filter(lambda dof: dofmap.local_to_global_index(dof) not in unowned,
                range(my_last-my_first))
    coords = F_dof_coordinates[list(dofs)]
    values[:] = interpfunction(coords)
    vec.set_local(values)
    vec.apply('insert')

def compute_gridcond(cutoff,initials,finals,Dx,Dy,Dz,x,y,z,dna,cond_base,conductivities,salt,BULK):
    INFTY = 10000
    
    if len(dna[0])>0:
        dnax = dna[0]
        dnay = dna[1]
        dnaz = dna[2]
    
        dist = INFTY * cond_base
        
        x_init, y_init, z_init = initials[0], initials[1], initials[2]
        x_fin, y_fin, z_fin = finals[0], finals[1], finals[2]
        
        xi = np.maximum(np.around((dnax-cutoff)/Dx)*Dx,x_init*np.ones_like(dnax))
        xi = np.where( np.abs(xi-dnax) < cutoff , np.maximum(xi-Dx,x_init*np.ones_like(dnax)), xi)
        xi_index=np.around((xi-x_init)/Dx).astype(int)
        xf = np.minimum(np.around((dnax+cutoff)/Dx)*Dx,x_fin*np.ones_like(dnax))
        xf = np.where( np.abs(xf-dnax) < cutoff , np.minimum(xf+Dx,x_fin*np.ones_like(dnax)), xf)
        # The plus one makes sure the indices include the final one.
        xf_index=np.around((xf-x_init)/Dx).astype(int)+1
        
        yi = np.maximum(np.around((dnay-cutoff)/Dy)*Dy,y_init*np.ones_like(dnay))
        yi = np.where( np.abs(yi-dnay) < cutoff , np.maximum(yi-Dy,y_init*np.ones_like(dnay)), yi)
        yi_index=np.around((yi-y_init)/Dy).astype(int)
        yf = np.minimum(np.around((dnay+cutoff)/Dy)*Dy,y_fin*np.ones_like(dnay))
        yf = np.where( np.abs(yf-dnay) < cutoff , np.minimum(yf+Dy,y_fin*np.ones_like(dnay)), yf)
        yf_index=np.around((yf-y_init)/Dy).astype(int)+1

        zi = np.maximum(np.around((dnaz-cutoff)/Dz)*Dz,z_init*np.ones_like(dnaz))
        zi = np.where( np.abs(zi-dnaz) < cutoff , np.maximum(zi-Dz,z_init*np.ones_like(dnaz)), zi)
        zi_index=np.around((zi-z_init)/Dz).astype(int)
        zf = np.minimum(np.around((dnaz+cutoff)/Dz)*Dz,z_fin*np.ones_like(dnaz))
        zf = np.where( np.abs(zf-dnaz) < cutoff , np.minimum(zf+Dz,z_fin*np.ones_like(dnaz)), zf)
        zf_index=np.around((zf-z_init)/Dz).astype(int)+1
        
        for i in range(0, len(dnax)):
            positions = np.array([[[[xx,yy,zz] for zz in (z[zi_index[i]:zf_index[i]])] for yy in (y[yi_index[i]:yf_index[i]])] for xx in (x[xi_index[i]:xf_index[i]])])
            distances = np.sqrt(np.sum(np.square(positions-dna.T[i]),axis=3))
            dist_section = dist[xi_index[i]:xf_index[i],yi_index[i]:yf_index[i],zi_index[i]:zf_index[i]]
            # the second condition does two things. First, it ensures that the walls where conductivity is zero remain as such. It does this because dist was first defined to be zero outside of the capillary, and so any value of d calculated above is guaranteed to not be less than zero. Therefore, the points outside the capillary always have a value of zero in the dist array. Secondly, it only rewrites the distance array if the presently calculated distance d is less than any previously calculated distance. In this way, it takes the minimum value of distance to the DNA beads.
            dist[xi_index[i]:xf_index[i],yi_index[i]:yf_index[i],zi_index[i]:zf_index[i]] = np.where((distances < cutoff) & (distances < dist_section),distances,dist_section)

# This is NOT creating an ndarray independent of dist, it's merely creating a new label that points to the same array dist. change cond, and you will also change dist.
        cond = dist
        cond[(cond < INFTY-1) & (cond > 0)] = combine(conductivities[salt],cond[(cond < INFTY-1) & (cond > 0)])
        cond[cond > INFTY-1] = BULK
        return cond
    return BULK * cond_base

def condval(X,Y,Z, mem_thick, box, p_radius):
    r = np.sqrt((X)**2+(Y)**2)

    cond_base=np.zeros_like(X)
    cond_base[np.where(r <= p_radius)] = 1 
    cond_base[np.where(Z < 0) ] = 1 
    cond_base[np.where(Z > mem_thick) ] = 1 
    
    return cond_base

def xyzrange(min_,max_,delta):
    return np.arange(min_-delta/2,max_+delta/2,delta)

def convert_array(arr,coor,vtd,grid):
    xp,yp,zp = [int(v)+1 for v in (coor[-1]-coor[0])/(grid)]
    # xp,yp,zp = [int(v) for v in (coor[-1]-coor[0])+1]
    a = np.zeros((xp,yp,zp))
    for i, dum in enumerate(coor):
        x,y,z = ((dum-coor[0])/grid).astype(int)
        # x,y,z = (dum-coor[0]).astype(int)
        a[x,y,z] = arr[vtd[i]]
    return a

# def convert_array(arr,coor,vtd):
#     xp = int(coor[-1][0]-coor[0][0]+1)
#     yp = int(coor[-1][1]-coor[0][1]+1)
#     zp = int(coor[-1][2]-coor[0][2]+1)
#     a = np.zeros((xp,yp,zp))
#     for i, dum in enumerate(coor):
#         x = int(dum[0]-coor[0][0])
#         y = int(dum[1]-coor[0][1])
#         z = int(dum[2]-coor[0][2])
#         a[x,y,z] = arr[vtd[i]]
#     return a


cutoff = 2.5
D = 1/2
Dx, Dy, Dz = D, D, D

mem_thick = 10
p_radius = 5
box = 50 
x = np.arange(-box/2, box/2 + Dx, Dx)
y = np.arange(-box/2, box/2 + Dy, Dy)
z = np.arange(-box, box + mem_thick + Dz, Dz)

X,Y,Z = np.meshgrid(x,y,z, indexing='ij')


initials = np.array([min(x),min(y),min(z)])
finals = np.array([max(x),max(y),max(z)])

numx = int((finals[0]-initials[0])/Dx)
numy = int((finals[1]-initials[1])/Dy)
numz = int((finals[2]-initials[2])/Dz)

solver = KrylovSolver("gmres", "amg")
parameters['krylov_solver']['nonzero_initial_guess'] = True
parameters["krylov_solver"]["monitor_convergence"] = True
solver.parameters["relative_tolerance"] = 1e-8
solver.parameters["maximum_iterations"] = 20000
set_log_level(30)

tol = 1E-14

mesh = BoxMesh(Point(initials), Point(finals),numx,numy,numz)

ground    = CompiledSubDomain('on_boundary && near(x[2],zwall,tol)', tol=tol, zwall = initials[2])
terminal  = CompiledSubDomain('on_boundary && near(x[2],zwall,tol)', tol=tol, zwall = finals[2])

boundary_parts = MeshFunction("size_t", mesh,mesh.topology().dim() - 1)
ground.mark(boundary_parts,1)
terminal.mark(boundary_parts,2)

ds = Measure('ds',domain=mesh, subdomain_data=boundary_parts)

#make Function Space
V = FunctionSpace(mesh, 'P', 1)
F = FunctionSpace(mesh, 'CG', 1)

bc_ground = DirichletBC(V, Constant(0), ground)
bc_terminal = DirichletBC(V, Constant(0.2), terminal)
bcs = [bc_ground, bc_terminal]

# define functions for variational problem
u = TrialFunction(V)
v = TestFunction(V)
sig = Function(F)
f= Constant(0)
DE = sig*dot(grad(u), grad(v))*dx - f*v*dx
a, L = lhs(DE), rhs(DE)

u1 = Function(V)

flux_top = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(2)
flux_bottom = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(1)


cond_base = condval(X,Y,Z, mem_thick, box, p_radius)
for salt in salts:
    #BULK = 17.3 #4M S/m LiCl 
    BULK = combine(conductivities[salt],cutoff)
    counter = 0
    with open("{}_{}_pull.txt".format(int(voltage*1000),step),"r") as fil:
        lines=fil.readlines()
        allvalues = np.array([ lines[i].split() for i in range(len(lines)) ],dtype=float)
        print("The min and max of any dna coordinate are: ",np.min(allvalues),np.max(allvalues))
        t = np.array(range(0,int(len(lines)/3)))*step*outputPeriod*stepsize/1000
         
        DATA = []
        for frame in range(int(len(lines)/3)):
            cond_time1 = time.time()
            
            if frame != 'empty':
                dna=np.array((lines[frame*3].split(),lines[frame*3+1].split(),lines[frame*3+2].split()),dtype=float)
            else:
                dna = [[],[],[]]
            cond = compute_gridcond(cutoff,initials,finals,Dx,Dy,Dz,x,y,z,dna,cond_base,conductivities,salt,BULK)
            
            intsig = RegularGridInterpolator((x,y,z),cond,bounds_error=False,fill_value=BULK)
            
            cond_time2 = time.time()
            print('Time for cond_gen: ',cond_time2-cond_time1)
            
            ######### FEniCS section
            fenics_time1 = time.time()

            loadFunc(mesh,F,sig,intsig)
            A,bb = assemble_system(a,L,bcs)
            solver.set_operator(A)
            
            # Solve Problem
            solver.solve( u1.vector(),bb)
            
            ft = assemble(flux_top)
            fb = assemble(flux_bottom)
            print("Top & Bottom:", ft,fb)

            # arr = u1.vector().get_local()
            # coor = mesh.coordinates()
            # vtd = vertex_to_dof_map(V)
            # pot = convert_array(arr,coor,vtd,D)
            # with open('V.'+str(frame)+'.pkl','wb') as Vfile:
            #     pickle.dump(pot,Vfile)

            
            fenics_time2 = time.time()
            
            print('Time for fenics: ',fenics_time2-fenics_time1)
            
            print('Total time: ', time.time() - start)
            
            DATA.append([t[counter], ft, fb])

            counter+=1

        print(DATA)
        with open("{}_{}.current.txt".format(int(voltage*1000),step),"w") as fl:
            for row in DATA:
                fl.write('{:e}\t{:e}\t{:e}\n'.format(row[0],row[1],row[2]))
