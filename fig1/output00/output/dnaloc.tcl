proc seq { s f {inc 1}} {
    # builds s..f
    if { $inc <= 0 } { error "seq: inc (third arg) must be positive!"; return }
    if { $s > $f } { set inc [expr -1*$inc] }
    for { } { ($f-$s)*$inc >= 0 } { set s [expr $s+$inc] } { lappend ret $s }
    return "$ret"
    }


set step 1000

set volt "200" 

mol new ../run.psf
mol addfile run.dcd waitfor all

set first 0
set last 12000

set frames [seq $first $last $step]

set d_cap [atomselect top "type D000"]
set condfile [open "${volt}_${step}_pull.txt" w]
foreach ff $frames {
    $d_cap frame $ff
    if {[$d_cap num] > 0} { 
        set x_cap [vecscale .1 [$d_cap get x]]
        set y_cap [vecscale .1 [$d_cap get y]]
        set z_cap [vecscale .1 [$d_cap get z]]
        
        puts $condfile $x_cap
        puts $condfile $y_cap
        puts $condfile $z_cap
    } else {
        puts $condfile ""
        puts $condfile ""
        puts $condfile ""
    } 
}
close $condfile
