import numpy as np
from scipy.interpolate import griddata
from gridData import Grid
from skimage.segmentation import find_boundaries
from writeDx import writeDx
from parseComsol import parseComsolData, parseComsolData3D

def replaceNaNWithWall(pot1, NaNvalues):
    pot=pot1
    for i in range(1,11):
        NaNTmp = np.zeros(np.shape(NaNvalues))
        NaNTmp[np.where(np.isnan(NaNvalues))] = -1
        tmp=find_boundaries(NaNTmp, mode='thick')
        print(len(tmp))
        idx = np.nonzero(tmp)
        NaNvalues[idx] = 0
        pot[idx] = 100*(i/2)**2
    return pot

dataFile = 'fig1_COMSOL.txt'
E_out  = 'electric.dx'
Barrier_out = 'confine.dx' 
print("Reading",dataFile)
comsolData = parseComsolData3D(dataFile)
x,y,z,fields = comsolData
dx,dy,dz = [a[1]-a[0] for a in (x,y,z)]
X,Y,Z = np.meshgrid(x,y,z, indexing='ij')

pot = fields[0]
# Charge reduction factor
eof = 0.25
electric = -1 * eof * 23.045131 * fields[0]

pot[np.where(~np.isnan(fields[0]))] = 0
barrier = np.zeros(np.shape(pot))
barrier[np.where(np.isnan(pot))] = 20000

barrier=replaceNaNWithWall(barrier, pot)

electric[np.where(np.isnan(electric))] = 0

delta = np.array( (dx,dy,dz) )
origin = np.array( (np.min(X),np.min(Y),np.min(Z)) )

sh0 = np.array(np.shape(pot))
print(delta)

sh1 = np.array(np.shape(pot))
print(sh0,sh1)
origin = origin + delta * ((sh0-sh1)/2)

writeDx( E_out, electric, origin = origin*10, delta = delta*10, fmt= '%.15f')
writeDx(Barrier_out, barrier, origin = origin*10, delta = delta*10, fmt= '%.8f')
