from mrdna import SingleStrandedSegment, DoubleStrandedSegment, SegmentModel
from mrdna.arbdmodel.coords import readArbdCoords
import numpy as np

steps1 =  60e6
steps2 =  60e6
steps3 =  60e6
steps4 =  30e8

tstep1 = 2e-4
tstep2 = 1e-4
tstep3 = 40e-6
tstep4 = tstep3


n1 = 25
n2 = 5
n3 = 5000/4999
n4 = n3

for run_num in range(1):
    dim = 5000 
    nbp = 500
    # Set up the model in the box.
##########################    
    segMain = DoubleStrandedSegment(
        name="helix1",
        start_position = np.array((-250,0,-250)), 
        end_position = np.array((250,0,-250)), 
##########################
        num_bp = nbp)

    model = SegmentModel( [segMain],
        local_twist = False,
        max_basepairs_per_bead = n1,
        max_nucleotides_per_bead = n1,
        dimensions=(dim,dim,dim))
    model.grid_potentials = [] # clear any existing potential

    model.add_grid_potential("confine.dx", scale=1.0, per_nucleotide = True)
    
    # Run the first coarse simulation, update the model spline with simulation results

    model.generate_bead_model(
        max_basepairs_per_bead = n1,
        max_nucleotides_per_bead = n1, 
        local_twist=False,
        escapable_twist=False)
    model.simulate(
        output_name = "loop1", 
        directory='output{:02d}' .format(run_num),
        num_steps=steps1, 
        output_period=1e4, 
        gpu = 1,
        timestep=tstep1)
    coords = readArbdCoords('output{:02d}/output/loop1.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    

    # Run the second finer simulation, update the coordinates again
    
    model.generate_bead_model(max_basepairs_per_bead = n2,
        max_nucleotides_per_bead = n2,
        local_twist=False, 
        escapable_twist=False)
    model.simulate(output_name = "loop2",
        directory='output{:02d}' .format(run_num),
        num_steps=steps2,
        output_period=1e4,
        gpu = 1,
        timestep=tstep2)
    coords = readArbdCoords('output{:02d}/output/loop2.restart' .format(run_num))
    model.update_splines(coords) # Fits a spline through the coordinates
    
    # Run the third finer simulation, update the coordinates again
    
    segMain = DoubleStrandedSegment(
        name="main",
        start_position = np.array((0,dim/4-500,0)), 
        end_position = np.array((0,-dim/4+500,0)), 
        num_bp = nbp)

    main = model.segments[0]

    final = SegmentModel([segMain],
        local_twist = True,
        max_basepairs_per_bead = n3,
        max_nucleotides_per_bead = n3,
        dimensions=(dim,dim,dim))

    print(final.segments)    

    final.grid_potentials = []
    final.add_grid_potential("confine.dx", scale=0.5, per_nucleotide = True)

    print(main)
    print(final.segments[0])
    #final.update_splines(coords)
    final.segments[0].position_spline_params = main.position_spline_params
    # if previous sim had twist
    #final.segments[0].quaternion_spline_params = main.quaternion_spline_params

    final.generate_bead_model(
        max_basepairs_per_bead = n3,
        max_nucleotides_per_bead = n3, 
        local_twist=True, 
        escapable_twist=False,
        one_bead_per_monomer = True)

    final.simulate(output_name = "loop3",
        directory='output{:02d}' .format(run_num),
        num_steps=steps3,
        output_period=1e4,
        gpu = 1,
        timestep=tstep3)
    coords = readArbdCoords('output{:02d}/output/loop3.restart' .format(run_num))
    final.update_splines(coords) # Fits a spline through the coordinates

    print(final.grid_potentials)
    
    accelerate = 1
    final.add_grid_potential("electric.dx", scale=1.0*accelerate, per_nucleotide = True)

    final.generate_bead_model(
        max_basepairs_per_bead = n4,
        max_nucleotides_per_bead = n4, 
        local_twist=True, 
        escapable_twist=False,
        one_bead_per_monomer = True)

    final.simulate(output_name = "run",
        directory='output{:02d}' .format(run_num),
        num_steps=steps4,
        output_period=1e4,
        gpu = 1,
        timestep=tstep4)


